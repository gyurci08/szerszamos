package Tools;

public abstract class Szerszam {
    private double hossz;
    private int szigeteles;
    private String SimpleName;

    public Szerszam(double hossz, int szigeteles) {
        this.hossz = hossz;
        this.szigeteles = szigeteles;
    }


    public double getHossz() {
        return hossz;
    }

    public void setHossz(double hossz) {
        this.hossz = hossz;
    }

    public int getSzigeteles() {
        return szigeteles;
    }

    public void setSzigeteles(int szigeteles) {
        this.szigeteles = szigeteles;
    }


    public abstract void szereles();
    public abstract void listazas();
    public String SimpleName(){
        return SimpleName;
    }
}
