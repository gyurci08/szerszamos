package Tools.Fogo;

public class CsipoFogo extends Fogo{
    public int getMaxVaghato() {
        return maxVaghato;
    }

    public void setMaxVaghato(int maxVaghato) {
        this.maxVaghato = maxVaghato;
    }

    private int maxVaghato;
    public CsipoFogo(double hossz, int szigeteles, int nyilasSzog, int maxVaghato) {
        super(hossz, szigeteles, nyilasSzog);
        setMaxVaghato(maxVaghato);

    }

    @Override
    public void szereles() {

    }

    @Override
    public void listazas() {

    }
}
