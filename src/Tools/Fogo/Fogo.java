package Tools.Fogo;

import Tools.Szerszam;

public abstract class Fogo extends Szerszam {
    public int getNyilasSzog() {
        return nyilasSzog;
    }

    public void setNyilasSzog(int nyilasSzog) {
        this.nyilasSzog = nyilasSzog;
    }

    private int nyilasSzog;
    public Fogo(double hossz, int szigeteles, int nyilasSzog) {
        super(hossz, szigeteles);
        setNyilasSzog(nyilasSzog);
    }
}
