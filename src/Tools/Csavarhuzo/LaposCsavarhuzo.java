package Tools.Csavarhuzo;

public class LaposCsavarhuzo extends Csavarhuzo{


    public double getFejMeret() {
        return fejMeret;
    }

    public void setFejMeret(double fejMeret) {
        this.fejMeret = fejMeret;
    }

    private double fejMeret;
    public LaposCsavarhuzo(double hossz, int szigeteles, double fejMeret) {
        super(hossz, szigeteles);
        setFejMeret(fejMeret);
    }

    @Override
    public void szereles() {

    }

    @Override
    public void listazas() {

    }
}
