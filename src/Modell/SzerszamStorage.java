package Modell;

import Tools.Szerszam;

import java.util.ArrayList;

public class SzerszamStorage {
    private  ArrayList <Szerszam> lista = new ArrayList<>();

    public Szerszam getLista(int i) {
        return lista.get(i);
    }

    public void addLista(Szerszam ujSzerszam) {
        lista.add(ujSzerszam);
    }

    public int getSize(){
        return lista.size();
    }



}
