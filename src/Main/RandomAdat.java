package Main;

import java.util.Random;

public class RandomAdat {
    private static final Random tipusRandom = new Random();


    public static int getRandomTipus(int bound){
        return tipusRandom.nextInt(bound);
    }


    private Random round;

    public RandomAdat(long seed){
        round = new Random(seed);
    }

    public int getInt(int min, int max) { return round.nextInt(max-min)+min;}
    public double getDouble(double min, double max) {return round.nextDouble(max-min)+min;}
    public String getString(String prefix, int min, int max) {return String.format("%s%d", prefix, round.nextInt(max-min)+min);}

}
