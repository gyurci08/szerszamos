package Modell;

import Tools.Csavarhuzo.LaposCsavarhuzo;
import Tools.Fogo.CsipoFogo;
import Tools.Fogo.KombinaltFogo;
import Tools.Szerszam;
import Main.RandomAdat;
public class SzerszamFactory {
    private static final int LENGTH_MIN = 5;
    private static final int LENGTH_MAX = 30;
    private static final int INSULATION_MIN = 1;
    private static final int INSULATION_MAX = 10;
    private static final int HEAD_MIN = 1;
    private static final int HEAD_MAX = 10;
    private static final int ANGLE_MIN = 20;
    private static final int ANGLE_MAX = 90;
    private static final double SURFACE_MIN = 0.7;
    private static final double SURFACE_MAX = 2;
    private static final int WIDTH_MIN = 1;
    private static final int WIDTH_MAX = 12;


    public static Szerszam ujCsipoFogo(){
      return  new CsipoFogo(1,30,2, 2);
    }


    public static Szerszam ujRandomSzerszam(RandomAdat[] generator){
        int tipus = RandomAdat.getRandomTipus(4);
        switch (tipus){
            case 0:
                return new LaposCsavarhuzo(
                        generator[tipus].getInt(LENGTH_MIN, LENGTH_MAX),
                        generator[tipus].getInt(INSULATION_MIN, INSULATION_MAX),
                        generator[tipus].getInt(HEAD_MIN, HEAD_MAX)
                );

            default:
                return new CsipoFogo(
                    generator[tipus].getInt(LENGTH_MIN, LENGTH_MAX),
                    generator[tipus].getInt(INSULATION_MIN, INSULATION_MAX),
                    generator[tipus].getInt(ANGLE_MIN, ANGLE_MAX),
                    generator[tipus].getInt(WIDTH_MIN, WIDTH_MAX)
            );



        }
    }


}
