package Tools.Fogo;

public class KombinaltFogo extends Fogo{
    public KombinaltFogo(double hossz, int szigeteles, int nyilasSzog, int fogasiFelulet) {
        super(hossz, szigeteles, nyilasSzog);
        this.fogasiFelulet = fogasiFelulet;
    }

    public int getFogasiFelulet() {
        return fogasiFelulet;
    }

    public void setFogasiFelulet(int fogasiFelulet) {
        this.fogasiFelulet = fogasiFelulet;
    }

    private int fogasiFelulet;
    private static final String SimpleName="Kombinált Fogó";




    @Override
    public void szereles() {
        System.out.println("préselés/szorítás/vágás");
    }

    @Override
    public void listazas() {
        System.out.println("Típus: "+SimpleName());
    }
}
